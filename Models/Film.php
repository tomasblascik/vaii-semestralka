<?php


class Film extends DBModel {

    // tabuľka
    public const TABLE = 'film';

    // stĺpce
    public const ID = "ID";
    public const NAME = "Name";
    public const TEXT = "Text";
    public const YEAR = "Year";


    /**
     * Vráti zoznam videí pre pagináciu so zvoleným limitom
     *
     * @param int $page - číslo stránky
     * @param int $limit - limit
     * @return array|null pole videí
     * [
     *     "count" => "celkový počet videí pre zadaný filter",
     *     "list" => [
     *          [
     *              "id" => "id videa",
     *              "name" => "názov videa",
     *              "text" => "text videa",
     *              "year" => "rok vydania videa"
     *          ],
     *          ...
     *     ]
     * ],
     * null ak došlo k chybe v komunikácii s databázou
     */
    public static function selectFilms(int $page, int $limit): ?array {

        $page = self::realEscapeString($page);

        $sql = "SELECT COUNT(*) AS rowCount FROM " . self::TABLE;

        $rowCount = 0;
        $result = self::query($sql);
        if ($result) {
            if ($result->num_rows > 0) {
                if ($row = $result->fetch_assoc()) {
                    $rowCount = $row['rowCount'];
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }


        $sql = "SELECT * FROM " . self::TABLE
            . " ORDER BY " . self::ID . " DESC"
            . " LIMIT " . $limit . " OFFSET " . (($page - 1) * $limit) . ";";

        $result = self::query($sql);

        $filmList = [];
        if ($result) {
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $filmList[] = $row;
                }
            }
            return [
                'count' => $rowCount,
                'list' => $filmList
            ];
        }
        return null;

    }

}