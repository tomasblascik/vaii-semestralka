<?php


abstract class DBModel {

    public const DB_HOST = 'localhost';
    public const DB_NAME = 'db1';
    public const DB_USER = 'root';
    public const DB_PASS = 'dtb456';

    private static $conn = NULL;

    private static final function createConnectionIfNotExists() {

        if (self::$conn === null) {

            $conn = new mysqli (
                self::DB_HOST,
                self::DB_USER,
                self::DB_PASS,
                self::DB_NAME
            );

            if ($conn->connect_error) {
                die('Model database connection failed.');
            } else {
                self::$conn = $conn;
            }

        }

    }

    protected static final function realEscapeString(string &$data): string {

        self::createConnectionIfNotExists();
        return self::$conn->real_escape_string($data);

    }

    protected static final function query(string $sql) {

        self::createConnectionIfNotExists();
        $result = self::$conn->query($sql);
        return $result;

    }

    public static final function beginTransaction(bool $write): bool {

        self::createConnectionIfNotExists();

        if ($write) {
            return self::$conn->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
        } else {
            return self::$conn->begin_transaction(MYSQLI_TRANS_START_READ_ONLY);
        }

    }

    public static final function commit(): bool {

        return self::$conn->commit();

    }

    public static final function rollback(): bool {

        return self::$conn->rollback();

    }

}