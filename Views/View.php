<?php


abstract class View {

    public function render() {

        include "Views/Components/Header.php";
        include "Views/Components/Menu.php"; ?>
        <div class="mainPart">
            <div class="backgroundPart">
                <?= $this->renderView(); ?>
            </div>
        </div> <?php
        include "Views/Components/Notification.php";
        include "Views/Components/Loader.php";
        include "Views/Components/Footer.php";

    }

}