<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Databáza filmov</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- vlastné štýly -->
    <link rel="stylesheet" href="Views/Styles/Main.css">
    <link rel="stylesheet" href="Views/Styles/Loader.css">
    <link rel="stylesheet" href="Views/Styles/Pagination.css">
    <link rel="stylesheet" href="Views/Styles/Notification.css">

    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

    <!-- vlastný js -->
    <script src="Views/JS/HamburgerMenu.js"></script>
    <script src="Views/JS/Pagination.js"></script>
    <script src="Views/JS/Notification.js"></script>
    <script src="Views/JS/Loader.js"></script>
    <script src="Views/JS/API.js"></script>
    <script src="Views/JS/DocumentReady.js"></script>

</head>
<body>