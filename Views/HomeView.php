<?php


class HomeView extends View {

    // kluce dat pre view
    public const FILM_COUNT = 'FILM_COUNT';
    public const FILM_LIST = 'FILM_LIST';

    // view data
    private $data = [];

    public function __construct(array $data) {
        $this->data = $data;
    }

    public function renderView() {?>

        <div class="pagination">
            <?php if ($this->data[self::FILM_COUNT] > 10):
                for($i = 1; $i <= ceil($this->data[self::FILM_COUNT] / 10); $i++): ?>
                    <button<?= $i === 1 ? ' class="active"' : ''; ?>><?= $i; ?></button>
                <?php endfor;
            endif; ?>
        </div>
        <div class="film-info">
            <span class="name">názov filmu</span>
            <span class="year">rok</span>
            <span class="category">kategória</span>
        </div>
        <div class="film-list">
            <?php foreach($this->data[self::FILM_LIST] as $films): ?>
                <a href="?p=film&id=<?= $films['ID']; ?>" class="film-item">
                    <span class="name"><?= $films[Film::NAME]; ?></span>
                    <span class="year"><?= $films[Film::YEAR]; ?></span>
                    <span class="category">kategória</span>
                </a>
            <?php endforeach;  ?>
        </div>

    <?php }

}