function API(loader, notification) {

    this.request = function(url, data, functionOK) {

        loader.show();

        $.ajax({
            url: url,
            method: 'POST',
            data: data
        }).done(function(data) {

            response = JSON.parse(data);
            switch(response.reply) {
                case 'ok':
                    functionOK(response);
                    loader.hide();
                    return;
                default:

            }
            loader.hide();
            notification.open(response.message);

        }).fail(function() {
            loader.hide();
            Notification.open('Chyba servera');
        });

    }

}