
var HamburgerMenu = new HamburgerMenu();
var Pagination = new Pagination();
var Notification = new Notification();
var Loader = new Loader();

var api = new API(loader, notification);

$(document).ready(function() {

    HamburgerMenu.init();
    Loader.init();
    Notification.init();
    Pagination.init();

});