function Notification() {

    var notification = null;
    var message = null;

    this.init = function() {

        $('.Notification .close').on('click', this.close);

        notification = $('.Notification');
        message = notification.find('span');

    }

    this.close = function() {

        if (!notification.hasClass('not-displayed')) {
            notification.addClass('not-displayed');
        }

    }

    this.open = function(text) {

        message.html(text);
        notification.removeClass('not-displayed');

    }

}