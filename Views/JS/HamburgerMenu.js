function HamburgerMenu() {


    this.init = function() {

        $('.menuHamburger').on('click', toggleMenu);

    }


    function toggleMenu() {

        var navBar = $('#myMenuNavBar');
        var menuIMG = $('#menuIMG');

        if (!navBar.hasClass('responsive')) {
            navBar.addClass('responsive');
            menuIMG.addClass('responsive');
        } else {
            navBar.removeClass('responsive');
            menuIMG.removeClass('responsive');
        }

    }


}