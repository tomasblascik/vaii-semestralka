function Pagination() {

    var pagination = null;

    var filmList = null;


    this.init = function() {

        pagination = $('.pagination');
        pagination.find('button').on('click', function() {
            selectPage($(this));
        });

        filmList = $('.film-list');

    }

    function selectPage(pageButton) {

        api.request(
            '?p=api&action=selectVideos',
            {
                page: pageButton.html(),
                limit: 10
            },
            function(response) {

                pagination.empty();

                for (var i = 1; i <= Math.ceil(response.message.count / 10); i++) {
                    pagination.append('<button>' + i + '</button>')
                }

                pagination.find('button').eq(pageButton.html() - 1).addClass('active');

                pagination.find('button').on('click', function() {
                    selectPage($(this));
                });



                filmList.empty();

                response.message.list.forEach(function(item) {
                    filmList.append(
                        '<a href="?p=film&id=' + item.id + '" class="film-item">'
                        + '<span class="name">' + item.name + '</span>'
                        + '<span class="year">' + item.year + '</span>'
                        + '<span class="category">' + 'kategória' + '</span>'
                        + '</a>'
                    );
                });

            }
        );

    }

}