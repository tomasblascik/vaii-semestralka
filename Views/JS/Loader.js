function Loader() {

    var loader = null;

    this.init = function() {

        loader = $('.nb-spinner');

    }

    this.show = function() {

        loader.removeClass('not-displayed');

    }

    this.hide = function() {

        if (!loader.hasClass('not-displayed')) {
            loader.addClass('not-displayed');
        }

    }

}