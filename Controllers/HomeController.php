<?php
require_once "Models/Film.php";
require_once "Views/HomeView.php";


class HomeController {

    public static function showFilmList() {

        $films = Film::selectFilms(1, 10);

        $data = [];
        $data[HomeView::FILM_COUNT] = $films['count'];
        $data[HomeView::FILM_LIST] = $films['list'];

        $view = new HomeView($data);
        $view->render();
    }

}