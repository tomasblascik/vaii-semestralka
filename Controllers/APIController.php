<?php
require_once "Models/Film.php";

class APIController {

    public const REPLY = 'reply';
    public const MESSAGE = 'message';

    public static function callAPI() {

        $action = isset($_GET['action']) ? $_GET['action'] : null;

        if ($action) {

            switch($action) {
                case 'selectFilms':
                    self::selectFilms();
                    break;
            }

        } else {
            echo self::reply([self::REPLY => "wrongMethod"]);
        }

    }

    private static function reply($reply) {
        echo json_encode($reply);
    }



    private static function selectVideos() {

        $films = Video::selectFilms($_POST['page'], $_POST['limit']);
        if ($films) {
            self::reply([
                self::REPLY => "ok",
                self::MESSAGE => $films
            ]);
        } else {
            self::reply([
                self::REPLY => "dbError",
                self::MESSAGE => "Nastala databázová chyba."
            ]);
        }

    }

}