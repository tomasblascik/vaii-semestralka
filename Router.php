<?php
require_once "Models/DBModel.php";
require_once "Views/View.php";
require_once "Controllers/APIController.php";
require_once "Controllers/HomeController.php";

class Router {

    public function start() {

        $page = isset($_GET['p']) ? $_GET['p'] : null;

        if ($page) {
            switch ($page) {
                case 'login':
                    echo "login";
                    break;
                case 'api';
                    APIController::callAPI();
                    break;
                default:
                    HomeController::showFilmList();
            }
        } else {
            HomeController::showFilmList();
        }

    }

}